var gulp        = require('gulp'),
    rename      = require('gulp-rename'),
    sass        = require('gulp-sass'),
    minifyCSS   = require('gulp-minify-css'),
    csso        = require('gulp-csso'),
    jade        = require('gulp-jade'),
    watch       = require('gulp-watch'),
    tinylr      = require('tiny-lr'),
    livereload  = require('gulp-livereload'),
    connect     = require('gulp-connect'),
    gutil       = require('gulp-util'),
    server      = tinylr();



gulp.task('jade',function(){
    return gulp.src('*.jade')
    .pipe(jade({
      pretty: true
    }))
    .on('error', gutil.log)
    .pipe(gulp.dest('.'))
    .pipe( livereload( server ));
});


gulp.task('sass', function() {
  return gulp.src('./assets/stylesheets/*.scss')
    .pipe( 
      sass( { 
        includePaths: ['./assets/stylesheets'],
        errLogToConsole: true
      } ) )
    .pipe( csso() )
    .pipe( gulp.dest('./assets/stylesheets/css') )
    .on('error', gutil.log)
    .pipe( livereload( server ));
});



gulp.task('css', function() {
  return gulp.src('./assets/stylesheets/css/*.css')
    .pipe(minifyCSS())
    .on('error', gutil.log)
    .pipe( gulp.dest('./assets/dist/css') )
    .pipe( livereload( server ));
});




gulp.task('watch', function () {
  server.listen(35729, function (err) {
    if (err) {
      return console.log(err);
    }
    gulp.watch('./assets/stylesheets/*.scss',['sass']);
    gulp.watch('./assets/stylesheets/css/*.css',['css']);
    gulp.watch('*.jade',['jade']);
  });
});



// Default Task
gulp.task('default', ['jade','watch','sass','css']);


